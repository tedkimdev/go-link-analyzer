package main

import (
	"fmt"
	"os"

	"github.com/tedkimdev/go-link-analyzer/crawler"
	"github.com/tedkimdev/go-link-analyzer/report"
)

func main() {
	if len(os.Args) < 2 {
		fmt.Println("no website provided")
		panic(0)
	}
	if len(os.Args) > 2 {
		fmt.Println("too many arguments provided")
		panic(0)
	}

	baseURL := os.Args[1]
	fmt.Printf("starting crawl of: %s...\n", baseURL)

	pages := make(map[string]int)
	pages = crawler.CrawlPage(baseURL, baseURL, pages)

	report.Print(pages)
}
