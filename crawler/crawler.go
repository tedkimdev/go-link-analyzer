package crawler

import (
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"

	"golang.org/x/net/html"
)

func CrawlPage(baseURL, currentURL string, pages map[string]int) map[string]int {
	// validate baseURL, currentURL
	parsedBaseURL, err := url.Parse(baseURL)
	if err != nil {
		log.Fatal(err)
	}
	parsedCurrentURL, err := url.Parse(currentURL)
	if err != nil {
		log.Fatal(err)
	}
	if parsedBaseURL.Host != parsedCurrentURL.Host {
		return pages
	}

	normalizedURL := normalizeURL(parsedCurrentURL.String())

	c, ok := pages[normalizedURL]
	if ok {
		pages[normalizedURL] = c + 1
		return pages
	} else {
		pages[normalizedURL] = 1
	}

	fmt.Println("crawling " + currentURL)

	resp, err := http.Get(currentURL)
	if err != nil {
		log.Fatal(err)
	}
	if resp.StatusCode > 399 {
		fmt.Println("Got HTTP error, status code: ", resp.Status)
		return pages
	}
	contentType := resp.Header.Get("content-type")
	if !strings.Contains(contentType, "text/html") {
		fmt.Println("Got non-html response: ", contentType)
		return pages
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}

	nextURLs := getURLsFromHTML(string(body), baseURL)
	if nextURLs == nil {
		return pages
	}

	for _, nextURL := range nextURLs {
		pages = CrawlPage(baseURL, nextURL.String(), pages)
	}

	return pages
}

func getURLsFromHTML(htmlBody string, baseURL string) []*url.URL {
	var urlStrings []string

	node, err := html.Parse(strings.NewReader(htmlBody))
	if err != nil {
		log.Println(err)
	}

	var crawl func(node *html.Node)
	crawl = func(node *html.Node) {
		if node.Type == html.ElementNode && node.Data == "a" {
			for _, attr := range node.Attr {
				if attr.Key == "href" {
					urlStrings = append(urlStrings, attr.Val)
					break
				}
			}
		}

		for child := node.FirstChild; child != nil; child = child.NextSibling {
			crawl(child)
		}
	}
	crawl(node)

	var urls []*url.URL
	for _, s := range urlStrings {
		url, err := stringToURL(s, baseURL)
		if err != nil {
			fmt.Printf("%s, %s\n", s, err.Error())
			continue
		}
		urls = append(urls, url)
	}

	return urls
}

func normalizeURL(rawURL string) string {
	parsedURL, err := url.Parse(rawURL)
	if err != nil {
		log.Fatal(err)
	}
	fullPath := fmt.Sprintf("%s%s", strings.ToLower(parsedURL.Host), strings.ToLower(parsedURL.Path))
	if len(fullPath) > 0 && strings.Split(fullPath, "")[len(fullPath)-1:][0] == "/" {
		fullPath = strings.Join(strings.Split(fullPath, "")[:len(fullPath)-1], "")
	}
	return fullPath
}

func stringToURL(urlString string, baseURL string) (*url.URL, error) {
	if urlString == "" {
		return nil, errors.New("failed to parse a url")
	}
	if strings.Split(urlString, "")[0] == "/" {
		return url.Parse(baseURL + urlString)
	}
	if strings.Contains(urlString, "https://") || strings.Contains(urlString, "http://") {
		return url.Parse(urlString)
	}
	return nil, errors.New("failed to parse a url")
}
