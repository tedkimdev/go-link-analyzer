package crawler

import "testing"

type normalizeURLTest struct {
	arg1, expected string
}

var normalizeURLTests = []normalizeURLTest{
	{"https://blog.boot.dev/path", "blog.boot.dev/path"},
	{"https://blog.boot.dev/path/", "blog.boot.dev/path"},
	{"https://BLOG.boot.dev/path", "blog.boot.dev/path"},
	{"http://blog.boot.dev/path", "blog.boot.dev/path"},
}

func TestNormalizeURL(t *testing.T) {
	for _, test := range normalizeURLTests {
		if output := normalizeURL(test.arg1); output != test.expected {
			t.Errorf("got %q, wanted %q", output, test.expected)
		}
	}
}

type getURLsFromHTMLTest struct {
	html     string
	baseURL  string
	expected []string
}

var getURLsFromHTMLTests = []getURLsFromHTMLTest{
	{`<html><body><a href="https://blog.boot.dev"><span>Boot.dev></span></a></body></html>`, "https://blog.boot.dev", []string{"https://blog.boot.dev"}},
	{`<html><body><a href="/path/one"><span>Boot.dev></span></a></body></html>`, "https://blog.boot.dev", []string{"https://blog.boot.dev/path/one"}},
	{`<html><body><a href="/path/one"><span>Boot.dev></span></a><a href="https://other.com/path/one"><span>Boot.dev></span></a></body></html>`, "https://blog.boot.dev", []string{"https://blog.boot.dev/path/one", "https://other.com/path/one"}},
	{`<html><body><a href="path/one"><span>Boot.dev></span></a></body></html>`, "https://blog.boot.dev", []string{}},
}

func TestGetURLsFromHTML(t *testing.T) {
	for j, test := range getURLsFromHTMLTests {
		outputs := getURLsFromHTML(test.html, test.baseURL)
		for i, output := range outputs {
			if output == nil && len(test.expected) == 0 {
				// success
			} else if output.String() != test.expected[i] {
				t.Errorf("got %q, wanted %q, %d", output, test.expected[i], j)
			}
		}
	}
}
