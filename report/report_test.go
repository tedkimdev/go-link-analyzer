package report

import "testing"

func TestSortPageKeysByCount(t *testing.T) {
	inputs := map[string]int{
		"url1": 5,
		"url2": 1,
		"url3": 3,
		"url4": 10,
		"url5": 7,
	}

	outputs := sortPageKeysByCount(inputs)
	wanted := []string{
		"url4",
		"url5",
		"url1",
		"url3",
		"url2",
	}
	for i, output := range outputs {
		if output != wanted[i] {
			t.Errorf("got %q, wanted %q", output, wanted[i])
		}
	}
}
func TestSortPageKeysByCount2(t *testing.T) {
	var inputs map[string]int
	outputs := sortPageKeysByCount(inputs)
	var wanted []string
	if len(outputs) != 0 || len(wanted) != 0 {
		t.Errorf("got %q, wanted %q", outputs, wanted)
	}
}
