package report

import (
	"fmt"
	"sort"
)

func Print(pages map[string]int) {
	fmt.Println("==========")
	fmt.Println("REPORT")
	fmt.Println("==========")
	sortedPageKeys := sortPageKeysByCount(pages)
	for _, key := range sortedPageKeys {
		fmt.Printf("Found %d internal links to %s\n", pages[key], key)
	}
}

func sortPageKeysByCount(pages map[string]int) []string {
	keys := make([]string, 0, len(pages))

	for key := range pages {
		keys = append(keys, key)
	}
	sort.SliceStable(keys, func(i, j int) bool {
		return pages[keys[i]] > pages[keys[j]]
	})
	return keys
}
